// projekt-nam0007.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <fstream>
#include "BinaryTree.h"

using namespace std;

const string SOURCE_DIR = "Sources/";



bool getKeyArray(int *&keyArray, unsigned int &size);

// nacitani ze souboru
bool loadKeyArray(string fileName, int *&keyArray, unsigned int &size);
// manualni zadavani
void inputKeyArray(int *&keyArray, unsigned int &size);


void printHeader(string text);

int main()
{
	int *keyArrays[] = { nullptr, nullptr };
	unsigned int keyArraySizes[2] = { 0, 0 };
	bool validKeyArays[2] = { false, false };


	printHeader("Prvni binarni strom");
	cout << endl;
	validKeyArays[0] = getKeyArray(keyArrays[0], keyArraySizes[0]);

	cout << endl << endl;

	printHeader("Druhy binarni strom");
	cout << endl;
	validKeyArays[1] = getKeyArray(keyArrays[1], keyArraySizes[1]);


	if (validKeyArays[0] && validKeyArays[1])
	{
		BinaryTree bt1(keyArrays[0], keyArraySizes[0]);
		BinaryTree bt2(keyArrays[1], keyArraySizes[1]);

		// vypis stromu
		cout << endl << endl;
		printHeader("Vypis binarnich stromu");
		cout << endl << "Prvni binarni strom:" << endl;
		bt1.PrintRecursiveInorder();

		cout << endl;

		cout << endl << "Druhy binarni strom:" << endl;
		bt2.PrintRecursiveInorder();
		cout << endl;

		// porovnani struktur stromu
		cout << endl << endl;
		printHeader("Porovnani struktur");
		cout << endl;


		if (BinaryTree::CompareStructures(bt1, bt2))
		{
			cout << "Binarni stromy maji stejnou strukturu." << endl;
		}
		else
		{
			cout << "Binarni stromy nemaji stejnou strukturu." << endl;
		}
	}
	else
	{
		cout << "Pri praci se soubory doslo k chybe." << endl;
	}

	cout << endl;

	// uvolneni pameti
	if (keyArrays[0] != nullptr)
		delete[] keyArrays[0];

	if (keyArrays[1] != nullptr)
		delete[] keyArrays[1];

    return 0;
}


bool getKeyArray(int *&keyArray, unsigned int &size)
{
	cout << "Jak chcete binarni strom vytvorit?" << endl;
	cout << "1 - nacist ze souboru" << endl;
	cout << "2 - zadat manualne" << endl;
	cout << endl << "Zvolte moznost: ";

	unsigned int choice;

	cin >> choice;
	while (choice != 1 && choice != 2)
	{
		cout << "Chybna volba. Zadejte znovu: ";
		cin >> choice;
	}

	cout << endl;

	switch (choice)
	{
	case 1:
	{
		string fileName;

		cout << "Zadejte nazev souboru, ze ktereho chcete nacist binarni strom: ";
		cin >> fileName;

		return loadKeyArray(SOURCE_DIR + fileName, keyArray, size);
	}
	case 2:
	{
		inputKeyArray(keyArray, size);
		break;
	}
	}

	return true;
}


bool loadKeyArray(string fileName, int *&keyArray, unsigned int &size)
{
	ifstream file;
	file.open(fileName);

	if (file.is_open())
	{
		int temp;
		size = 0;

		// zisteni pocu radku
		while (!file.eof())
		{
			file >> temp;
			size++;
		}

		file.clear();
		file.seekg(0, file.beg);

		keyArray = new int[size];

		int i = 0;
		while (!file.eof())
		{
			file >> temp;
			keyArray[i] = temp;
			i++;
		}

		return true;
	}

	return false;
}


void inputKeyArray(int *&keyArray, unsigned int &size)
{
	cout << "Zadejte pocet uzlu binarniho stromu: ";
	cin >> size;

	keyArray = new int[size];

	for (unsigned int i = 0; i < size; i++)
	{
		cout << i+1 << ". uzel: ";
		cin >> keyArray[i];
	}
}


void printHeader(string text)
{
	size_t textLenght = text.length();
	string separator = "";

	for (size_t i = 0; i < textLenght; i++)
	{
		separator += "=";
	}

	cout << separator << endl;
	cout << text << endl;
	cout << separator << endl;
}